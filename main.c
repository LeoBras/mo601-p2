#include <linux/types.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <elf.h>

#define BIT(x)  ( 1 << (x) )
#define MASK(x) ( BIT(x) - 1 )

#define PRINT_LOG 1
#define STACK_SIZE 0x500000

typedef __u32 u32;
typedef __s32 s32;
typedef __u64 u64;
typedef __s64 s64;

struct rtype {
	u32 opcode	: 7;
	u32 rd		: 5;
	u32 funct3	: 3;
	u32 rs1		: 5;
	u32 rs2		: 5;
	u32 funct7	: 7;
};

struct btype {
	u32 opcode	: 7;
	u32 imm11	: 1;
	u32 imm4_1	: 4;
	u32 funct3	: 3;
	u32 rs1		: 5;
	u32 rs2		: 5;
	u32 imm10_5	: 6;
	u32 imm12	: 1;
};

struct itype {
	u32 opcode	: 7;
	u32 rd		: 5;
	u32 funct3	: 3;
	u32 rs1		: 5;
	u32 imm12	: 12;
};

struct stype {
	u32 opcode	: 7;
	u32 imm5	: 5;
	u32 funct3	: 3;
	u32 rs1		: 5;
	u32 rs2		: 5;
	u32 imm7	: 7;
};

struct utype {
	u32 opcode	: 7;
	u32 rd		: 5;
	u32 imm20	: 20;
};

struct jtype {
	u32 opcode	: 7;
	u32 rd		: 5;
	u32 imm19_12	: 8;
	u32 imm11	: 1;
	u32 imm10_1	: 10;
	u32 imm20	: 1;
};

struct instr {
	union {
		u32 opcode : 7;
		struct rtype r;
		struct btype b;
		struct itype i;
		struct stype s;
		struct utype u;
		struct jtype j;
		u32 data;
	};
};

s32 sign_extend(u32 value, u32 bits)
{
	if (value & BIT(bits - 1)) /* sign bit set */
		value |= ~(MASK(bits));
	else
		value &= MASK(bits);

	return value;
}


static u32 x[32];
static u32 pc;
static u64 clk;
static u32 *mem_offset;

enum e_instr_type {
	R_TYPE,
	B_TYPE,
	I_TYPE,
	S_TYPE,
	U_TYPE,
	J_TYPE,
};

struct print_data {
	u32 pc_start;
	u32 prev_rs1;
	u32 prev_rs2;
	enum e_instr_type inst_type;
	char *mnem;
	s32 imm;
};

enum e_load{
	LB,
	LH,
	LW,
	LBU = 4,
	LHU
};

void load(struct itype i, struct print_data *pd)
{
	u32 *addr;
	u32 value;

	if(i.rd == 0)
		return;

	pd->imm = sign_extend(i.imm12, 12);
	addr = (void *)mem_offset + x[i.rs1] + pd->imm;
	value = *addr;

	switch (i.funct3) {
	case LB:
		value = sign_extend(value, 8);
		pd->mnem = "lb";
		break;
	case LH:
		value = sign_extend(value, 16);
		pd->mnem = "lh";
		break;
	case LW:
		/* No change */
		pd->mnem = "lw";
		break;
	case LBU:
		value &= MASK(8);
		pd->mnem = "lbu";
		break;
	case LHU:
		value &= MASK(16);
		pd->mnem = "lhu";
		break;
	default:
		printf("Error: load function with funct3 = %d not implemented\n",
		       i.funct3);
		exit(-1);
		break;
	}

	x[i.rd] = value;


}

enum e_misc_mem{
	PAUSE = 0x010,
	FENCE_TSO = 0x833,
};

void misc_mem(struct itype i, struct print_data *pd)
{
	/*In order processor, nothing needs to be done*/

	switch(i.imm12) {
	case PAUSE:
		pd->mnem = "pause";
		break;
	case FENCE_TSO:
		pd->mnem = "fence.tso";
		break;
	default:
		pd->mnem = "fence";
		break;
	}
}

enum e_op_imm {
	ADDI,
	SLLI,
	SLTI,
	SLTIU,
	XORI,
	SRLI_SRAI,
	ORI,
	ANDI
};

enum e_func7_types {
	REGULAR   = 0,
	MULDIV    = 1,
	ALTERNATE = 0x20
};

void op_imm(struct itype i, struct print_data *pd)
{
	char * const mnem[] = {
		"addi", "slli", "slti", "sltiu", "xori", "srli", "ori", "andi"
	};

	s32 imm = sign_extend(i.imm12, 12);

	if (i.funct3 == SRLI_SRAI || i.funct3 == SLLI)
		pd->imm = i.imm12 & MASK(5);
	else
		pd->imm = imm;
	pd->mnem = mnem[i.funct3];

	/* Fast path: go out if destination = x0.*/

	if (i.rd == 0) {
		if (i.funct3 == SRLI_SRAI && (i.imm12 >> 5) == ALTERNATE)
			pd->mnem = "srai";
		return;
	}

	switch (i.funct3) {
	case ADDI:
		x[i.rd] = x[i.rs1] + imm;
		break;
	case SLLI:
		x[i.rd] = x[i.rs1] << pd->imm ;
		break;
	case SLTI:
		s32 tmp = (s32)x[i.rs1];
		if (tmp < imm)
			x[i.rd] = 1;
		else
			x[i.rd] = 0;
		break;
	case SLTIU:
		if (x[i.rs1] < (u32)imm)
			x[i.rd] = 1;
		else
			x[i.rd] = 0;
		break;
	case XORI:
		x[i.rd] = x[i.rs1] ^ imm;
		break;
	case SRLI_SRAI:
		u32 code = i.imm12 >> 5;

		if (code == 0) { //SRLI
			x[i.rd] = x[i.rs1] >> pd->imm;
		} else if (code == ALTERNATE) { //SRAI
			x[i.rd] = ((x[i.rs1] & ~(BIT(31))) >> pd->imm) |
				  x[i.rs1] & BIT(31);
			pd->mnem = "srai";
		} else {
			printf("Error: SRLI/SRAI code %d not implemented\n",
			       code);
			exit(-1);
		}
		break;
	case ORI:
		x[i.rd] = x[i.rs1] | imm;
		break;
	case ANDI:
		x[i.rd] = x[i.rs1] & imm;
		break;
	}
}

void auipc(struct utype u, struct print_data *pd)
{
	if (u.rd != 0)
		x[u.rd] = pc + (u.imm20 << 12);

	pd->mnem = "auipc";
	pd->imm = u.imm20;
}

enum e_store {
	SB,
	SH,
	SW
};

void store(struct stype s, struct print_data *pd)
{
	s32 imm;
	u32 value = x[s.rs2];
	u32 *addr;

	imm = sign_extend(s.imm5 | (s.imm7 << 5), 12);
	addr = (void *)mem_offset + x[s.rs1] + imm;

	switch (s.funct3) {
	case SB:
		value = (*addr & (~MASK(8))) | (value & MASK(8));
		pd->mnem = "sb";
		break;
	case SH:
		value = (*addr & (~MASK(16))) | (value & MASK(16));
		pd->mnem = "sh";
		break;
	case SW:
		pd->mnem = "sw";
		break;
	}

	*addr = value;
	pd->imm = imm;
}

enum e_muldiv_op {
	MUL,
	MULH,
	MULHSU,
	MULHU,
	DIV,
	DIVU,
	REM,
	REMU
};

void muldiv(struct rtype r)
{
	s64 tmp_s;
	u64 tmp_u;

	/* Fastpath rs1 = 0 -> result = 0 */
	if (x[r.rs1] == 0) {
		x[r.rd] = 0;
		return;
	}

	/* Fastpath rs2 = 0 -> mul = 0  & div/rem error */
	if (x[r.rs2] == 0) {
		if (r.funct3 <= MULHU) {
			x[r.rd] = 0;
			return;
		}

		printf("Error: div/rem division by 0: program stop\n");
		exit(-1);
	}

	switch (r.funct3) {
	case MUL:
		tmp_s = (s64)((s32)x[r.rs1] * (s32)x[r.rs2]);
		tmp_s = (tmp_s & MASK(31)) | ((tmp_s >> 32) & BIT(31));
		x[r.rd] = (s32)tmp_s;
		break;
	case MULH:
		tmp_s = (s32)x[r.rs1] * (s32)x[r.rs2];
		x[r.rd] = (s32)(tmp_s >> 32);
		break;
	case MULHSU:
		tmp_s = (s32)x[r.rs1] * (u32)x[r.rs2];
		x[r.rd] = (s32)(tmp_s >> 32);
		break;
	case MULHU:
		tmp_u = (u32)x[r.rs1] * (u32)x[r.rs2];
		x[r.rd] = (u32)(tmp_u >> 32);
		break;
	case DIV:
		tmp_s = (s32)x[r.rs1] / (s32)x[r.rs2];
		x[r.rd] = (s32) tmp_s;
		break;
	case DIVU:
		tmp_u = (u32)x[r.rs1] / (u32)x[r.rs2];
		x[r.rd] = (u32) tmp_u;
		break;
	case REM:
		tmp_s = (s32)x[r.rs1] % (s32)x[r.rs2];
		x[r.rd] = (s32) tmp_s;
		break;
	case REMU:
		tmp_u = (u32)x[r.rs1] % (u32)x[r.rs2];
		x[r.rd] = (u32) tmp_s;
		break;
	}
}


void print_op(struct rtype r, struct print_data *pd) {
	char * const mnem[] = {
	    "add", "sll", "stl", "sltu", "xor", "srl", "xor", "and"
	};

	char * const mnem_muldiv[] = {
	    "mul", "mulh", "mulhsu", "mulhu", "div", "divu", "rem", "remu"
	};

	char * const mnem_alt[] = {
	    "sub", "sll", "stl", "sltu", "xor", "sra", "xor", "and"
	};

	switch(r.funct7) {
	case REGULAR:
		pd->mnem = mnem[r.funct3];
		break;
	case MULDIV:
		pd->mnem = mnem_muldiv[r.funct3];
		break;
	case ALTERNATE:
		pd->mnem = mnem_alt[r.funct3];
		break;
	default:
		printf("Error: invalid instruction funct7 = %d\n",
		       r.funct7);
		exit(-1);
	}
}

enum e_op {
	ADD_SUB,
	SLL,
	SLT,
	SLTU,
	XOR,
	SRL_SRA,
	OR,
	AND
};

void op(struct rtype r, struct print_data *pd)
{
	print_op(r, pd);

	/* Fast path: go out if destination = x0.*/
	if (r.rd == 0) {
		return;
	}

	if (r.funct7 == MULDIV) {
		muldiv(r);
		return;
	}

	switch (r.funct3) {
	case ADD_SUB:
		if (r.funct7 == 0) { //Add
			x[r.rd] = x[r.rs1] + x[r.rs2];
		} else if (r.funct7 == ALTERNATE) { //Sub
			x[r.rd] = x[r.rs1] - x[r.rs2];
		} else {
			printf("Error: add/sub with funct7 = %d not implemented\n",
			       r.funct7);
			exit(-1);
		}
		break;
	case SLL:
		x[r.rd] = x[r.rs1] << (x[r.rs2] & MASK(5));
		break;
	case SLT:
		if ((s32)x[r.rs1] < (s32)x[r.rs2])
			x[r.rd] = 1;
		else
			x[r.rd] = 0;
		break;
	case SLTU:
		if (x[r.rs1] < x[r.rs2])
			x[r.rd] = 1;
		else
			x[r.rd] = 0;
		break;
	case XOR:
		x[r.rd] = x[r.rs1] ^ x[r.rs2];
		break;
	case SRL_SRA:
		if (r.funct7 == 0) { //SRL
			x[r.rd] = x[r.rs1] >> (x[r.rs2] & MASK(5));
		} else if (r.funct7 == ALTERNATE) { //SRA
			x[r.rd] = ((x[r.rs1] & ~(BIT(31))) >> (x[r.rs2] & MASK(5)));
			x[r.rd] |= x[r.rs1] & BIT(31);
		} else {
			printf("Error: SRL/SRA function with funct7 = %d not implemented\n",
			       r.funct7);
			exit(-1);
		}
		break;
	case OR:
		x[r.rd] = x[r.rs1] | x[r.rs2];
		break;
	case AND:
		x[r.rd] = x[r.rs1] & x[r.rs2];
		break;
	}
}

void lui(struct utype u, struct print_data *pd)
{
	if (u.rd != 0)
		x[u.rd] = u.imm20 << 12;

	pd->mnem = "lui";
	pd->imm = u.imm20;
}

enum e_branch {
	BEQ,
	BNE,
	BLT = 4,
	BGE,
	BLTU,
	BGEU
};

bool branch(struct btype b, struct print_data *pd)
{
	s32 imm;
	bool jump = false;

	imm  = b.imm4_1  << 1;
	imm |= b.imm10_5 << 5;
	imm |= b.imm11   << 11;
	imm |= b.imm12   << 12;
	imm = sign_extend(imm, 13);
	pd->imm = imm;

	switch (b.funct3) {
	case BEQ:
		jump = (x[b.rs1] == x[b.rs2]);
		pd->mnem = "beq";
		break;
	case BNE:
		jump = (x[b.rs1] != x[b.rs2]);
		pd->mnem = "bne";
		break;
	case BLT:
		jump = ((s32)x[b.rs1] < (s32)x[b.rs2]);
		pd->mnem = "blt";
		break;
	case BGE:
		jump = ((s32)x[b.rs1] >= (s32)x[b.rs2]);
		pd->mnem = "bge";
		break;
	case BLTU:
		jump = (x[b.rs1] < x[b.rs2]);
		pd->mnem = "bltu";
		break;
	case BGEU:
		jump = (x[b.rs1] >= x[b.rs2]);
		pd->mnem = "bgeu";
		break;
	}

	if (jump)
		pc += imm;

	return jump;
}

void jalr(struct itype i, struct print_data *pd)
{
	s32 imm = sign_extend(i.imm12, 12);

	if (i.rd != 0)
		x[i.rd] = pc + 4;

	pc = (x[i.rs1] + imm) & ~(BIT(0));

	pd->imm = imm;
	pd->mnem = "jalr";
}

void jal(struct jtype j, struct print_data *pd)
{
	s32 imm;

	// Save PC return if rd != r0
	if (j.rd != 0)
		x[j.rd] = pc + 4;

	imm  = j.imm10_1  << 1;
	imm |= j.imm11    << 11;
	imm |= j.imm19_12 << 12;
	imm |= j.imm20    << 20;

	imm = sign_extend(imm, 21);

	pc += imm;

	pd->imm = pc;
	pd->mnem = "jal";
}

enum e_system_op{
	ECALL,
	EBREAK
};

bool system_op(struct itype i, struct print_data *pd)
{
	switch(i.imm12) {
	case EBREAK:
		pd->mnem = "ebreak";
		return true;
	case ECALL:
	default:
		pd->mnem = "ecall";
		printf("Error! System instructions other than EBREAK were not implemented\n");
		exit(-1);
	}
}

enum opcode {
	LOAD	= 0x03,
	MISC_MEM= 0x0F,
	OP_IMM	= 0x13,
	AUIPC	= 0x17,
	STORE	= 0x23,
	OP	= 0x33,
	LUI	= 0x37,
	BRANCH	= 0x63,
	JALR	= 0x67,
	JAL	= 0x6F,
	SYSTEM	= 0x73,
};

void print_log(struct instr ins, struct print_data *pd)
{
	static char buffer[50];
	const char *rabi[] = {
	    "zero",	"ra",	"sp",   "gp",
	    "tp",	"t0",	"t1",	"t2",
	    "s0",	"s1",	"a0",	"a1",
	    "a2",	"a3",	"a4",	"a5",
	    "a6",	"a7",	"s2",	"s3",
	    "s4",	"s5",	"s6",	"s7",
	    "s8",	"s9",	"s10",	"s11",
	    "t3",	"t4",	"t5",	"t6",
	};

	switch(pd->inst_type) {
	case R_TYPE:
		sprintf(buffer, "%-8s %s,%s,%s",
			pd->mnem, rabi[ins.r.rd], rabi[ins.r.rs1], rabi[ins.r.rs2]);
		break;
	case B_TYPE:
		sprintf(buffer, "%-8s %s,%s,%d",
			pd->mnem, rabi[ins.b.rs1], rabi[ins.b.rs2], pd->imm);
		break;
	case I_TYPE:
		switch (ins.i.opcode) {
		case SYSTEM:
		case MISC_MEM:
			sprintf(buffer, "%-8s", pd->mnem);
			break;
		case LOAD:
			sprintf(buffer, "%-8s %s,%d(%s)",
				pd->mnem, rabi[ins.i.rd], pd->imm, rabi[ins.i.rs1]);
			break;
		case JALR:
			sprintf(buffer, "%-8s %d(%s)",
				pd->mnem, pd->imm, rabi[ins.i.rs1]);
			break;
		default:
			sprintf(buffer, "%-8s %s,%s,%d",
				pd->mnem, rabi[ins.i.rd], rabi[ins.i.rs1], pd->imm);
		}
		break;
	case S_TYPE:
		sprintf(buffer, "%-8s %s,%d(%s)",
			pd->mnem, rabi[ins.s.rs2], pd->imm, rabi[ins.s.rs1]);
		break;
	case U_TYPE:
		sprintf(buffer, "%-8s %s,0x%x",
			pd->mnem, rabi[ins.u.rd], (u32)pd->imm);
		break;
	case J_TYPE:
		sprintf(buffer, "%-8s %s,0x%x",
			pd->mnem, rabi[ins.j.rd], pd->imm);
		break;
	}

	printf("PC=%010x [%010x] x%02d=%010x x%02d=%010x x%02d=%010x %s \n",
	       pd->pc_start, ins.data,
	       ins.r.rd, x[ins.r.rd],
	       ins.r.rs1, pd->prev_rs1,
	       ins.r.rs2, pd->prev_rs2,
	       buffer);
}

bool run_inst(struct instr ins)
{
	bool jumped = false;
	bool end = false;

	struct print_data pd = {
		.pc_start = pc,
		.prev_rs1 = x[ins.r.rs1],
		.prev_rs2 = x[ins.r.rs2],
	};

	switch(ins.opcode) {
	case LOAD:
		load(ins.i, &pd);
		pd.inst_type = I_TYPE;
		break;
	case MISC_MEM:
		misc_mem(ins.i, &pd);
		pd.inst_type = I_TYPE;
		break;
	case OP_IMM:
		op_imm(ins.i, &pd);
		pd.inst_type = I_TYPE;
		break;
	case AUIPC:
		auipc(ins.u, &pd);
		pd.inst_type = U_TYPE;
		break;
	case STORE:
		store(ins.s, &pd);
		pd.inst_type = S_TYPE;
		break;
	case OP:
		op(ins.r, &pd);
		pd.inst_type = R_TYPE;
		break;
	case LUI:
		lui(ins.u, &pd);
		pd.inst_type = U_TYPE;
		break;
	case BRANCH:
		jumped = branch(ins.b, &pd);
		pd.inst_type = B_TYPE;
		break;
	case JALR:
		jalr(ins.i, &pd);
		pd.inst_type = I_TYPE;
		jumped = true;
		break;
	case JAL:
		jal(ins.j, &pd);
		pd.inst_type = J_TYPE;
		jumped = true;
		break;
	case SYSTEM:
		end = system_op(ins.i, &pd);
		pd.inst_type = I_TYPE;
		break;
	default:
		printf("Error: instruction opcode = %d not implemented\n",
		       ins.opcode);
		exit(-1);
	}

	if (!jumped)
		pc += 4;

	clk++;

	if (PRINT_LOG)
		print_log(ins, &pd);

	return end;
}

void fread_offset(FILE *file, long offset, void *out_buf, size_t sz, size_t n)
{
	if (fseek(file, offset, SEEK_SET) < 0) {
		perror("Error seeking elf file");
		exit(-4);
	}

	if (fread(out_buf, sz, n, file) == 0) {
		printf("Error reading elf file at offset %ld \n", offset);
		exit(-4);
	}
}

void load_elf(char *filename)
{
	Elf32_Ehdr elf_header;	//Elf header
	Elf32_Phdr *ph_table;	//Elf Program header table

	FILE *elf;
	u32 last = 0;

	elf = fopen(filename, "r");
	if (!elf) {
		printf("Error: Could not open file %s\n", filename);
		exit(-3);
	}

	/* ELF HEADER */

	fread_offset(elf, 0, &elf_header, sizeof(Elf32_Ehdr), 1);
	if (elf_header.e_machine != EM_RISCV) {
		printf("Error: ISA not for RISC-V\n");
		exit(-3);
	}

	if (elf_header.e_ident[EI_CLASS] != ELFCLASS32) {
		printf("Error: Wrong ELF class (not 32-bit)\n");
		exit(-3);
	}

	if (elf_header.e_ident[EI_DATA] != ELFDATA2LSB) {
		printf("Error: Wrong ELF endianess (not little endian)\n");
		exit(-3);
	}

	pc = elf_header.e_entry;

	/* Program Header Table */

	ph_table = calloc(elf_header.e_phnum, elf_header.e_phentsize);
	fread_offset(elf, elf_header.e_phoff, ph_table,
		     elf_header.e_phentsize, elf_header.e_phnum);

	/*
	 * Max memory size = STACK_SIZE
	 *
	 * It could easily become multiple memory regions, but since the
	 * expected size of the code/data is very small, I did not bother to.
	 */

	mem_offset = calloc(STACK_SIZE, 1);

	for(int i = 0; i < elf_header.e_shnum; i++){
		if(ph_table[i].p_type != PT_LOAD)
			continue;

		if (ph_table[i].p_filesz == 0)
			continue;

		fread_offset(elf, ph_table[i].p_offset,
			     (void *)mem_offset + ph_table[i].p_vaddr,
			     ph_table[i].p_filesz, 1);
	}

	fclose(elf);
}

uint64_t rdtsc(void){
	unsigned int lo,hi;
	__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
	return ((uint64_t)hi << 32) | lo;
}

int main (int argc, char **argv)
{
	u64 host_clk;

	if (argc != 2) {
		printf("Usage: %s <elfname>\n", argv[0]);
		return -2;
	}

	load_elf(argv[1]);

	host_clk = rdtsc();
	while(true){
		struct instr ins;
		u32 *local_pc = (void *)mem_offset + pc;

		ins.data = *local_pc;
		if (run_inst(ins))
			break;
	}

	host_clk = rdtsc() - host_clk;

	fprintf(stderr, "sim_clk = %llu, clk_host = %llu, host/sim = %f\n",
		clk, host_clk, 1.0 * host_clk / clk);

	free(mem_offset);
	return 0;
}
