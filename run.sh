#!/bin/bash

acstone() {
#Compile ACStone
  git submodule init
  git submodule update
  cd ACStone/
  source acstone.env.sh
  bash compilar.sh

  # Move .riscv files to test/
  cd ..
  mkdir test
  mv ACStone/*riscv test/
}

build() {
# Build rv32im
  gcc -o rv32im main.c -O9 -Wno-overflow
}

simulate() {
# Run all tests
  for f in test/*riscv ; do
    echo $f
    ./rv32im $f > test/$(basename $f .riscv).log
  done
}

if test -z $@ ; then
  acstone
  build
  simulate

  exit
fi

for cmd in $@; do
  $cmd
done
